package com.empresas.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConfiguracaoRetrofit {

    private static ConfiguracaoRetrofit mInstance;
    private final Retrofit retrofit;
    public static final String URL = "http://empresas.ioasys.com.br/api/v1/";

    public ConfiguracaoRetrofit(){

        this.retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized ConfiguracaoRetrofit getmInstance(){
        if(mInstance == null){
            mInstance = new ConfiguracaoRetrofit();
        }
        return mInstance;
    }

    public LoginService getLoginService(){
        return retrofit.create(LoginService.class);
    }
}
