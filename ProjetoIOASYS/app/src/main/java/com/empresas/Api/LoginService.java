package com.empresas.Api;

import com.empresas.models.Investor;
import com.empresas.models.RespostaLogin;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LoginService {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    Call<RespostaLogin> login(
            @Field("email") String email,
            @Field("password") String senha
    );

    @FormUrlEncoded
    @GET("users/auth/sign_in")
    Call<RespostaLogin> respostaLogin(
            @Path("Investor") Investor investor
    );

}
