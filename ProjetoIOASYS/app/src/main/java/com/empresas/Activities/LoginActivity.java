package com.empresas.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.empresas.Api.ConfiguracaoRetrofit;
import com.empresas.R;
import com.empresas.models.AcessoInfo;
import com.empresas.models.Investor;
import com.empresas.models.RespostaLogin;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    //Serviço
    private RespostaLogin respostaLogin;
    private Investor investor;
    private AcessoInfo acessoInfo;

    // UI references.
    private EditText etEmail;
    private EditText etSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = (EditText) findViewById(R.id.etEmail);

        etSenha = (EditText) findViewById(R.id.etSenha);

        etSenha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    tentaLogin();
                    return true;
                }
                return false;
            }
        });

        Button btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                tentaLogin();
            }
        });
    }

    private void tentaLogin() {

        // Reset errors.
        etEmail.setError(null);
        etSenha.setError(null);

        // Store values at the time of the login attempt.
        String email = etEmail.getText().toString().trim();
        String senha = etSenha.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        //validação campos email e senha
        if (TextUtils.isEmpty(email)) {
            etEmail.setError(getString(R.string.error_field_required));
            focusView = etEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            etEmail.setError(getString(R.string.error_invalid_email));
            focusView = etEmail;
            cancel = true;
        }

        if (TextUtils.isEmpty(senha)) {
            etSenha.setError(getString(R.string.error_field_required));
            focusView = etSenha;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            //requisicao
            Call<RespostaLogin> requisicao = ConfiguracaoRetrofit
                    .getmInstance()
                    .getLoginService()
                    .login(email, senha);

            requisicao.enqueue(new Callback<RespostaLogin>() {
                @Override
                public void onResponse(Call<RespostaLogin> call, Response<RespostaLogin> response) {

                    try {
                        respostaLogin = response.body();
                        investor = respostaLogin.getInvestor();
                        acessoInfo = new AcessoInfo(response);

                    } catch (Exception e){

                    }

                    if (respostaLogin == null) {
                        Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_LONG).show();
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    }
                }

                @Override
                public void onFailure(Call<RespostaLogin> call, Throwable t) {

                }
            });
//            mAuthTask = new UserLoginTask(email, senha);
//            mAuthTask.execute((Void) null);
        }


    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

}

