package com.empresas.models;

import com.google.gson.annotations.SerializedName;

import retrofit2.Response;

public class AcessoInfo {

    @SerializedName("access-token")
    private String access_token;

    @SerializedName("client")
    private String client;

    @SerializedName("uid")
    private String uid;

    public AcessoInfo(Response<RespostaLogin> response) {
        this.access_token = response.headers().get("access-token");
        this.client = response.headers().get("client");
        this.uid = response.headers().get("uid");
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getClient() {
        return client;
    }

    public String getUid() {
        return uid;
    }
}
