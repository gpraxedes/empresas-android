package com.empresas.models;

public class Investor {

    private int id;
    private String investor_name;
    private String email;
    private String city;


    public Investor(int id, String investor_name, String email, String city) {
        this.id = id;
        this.investor_name = investor_name;
        this.email = email;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }
}
