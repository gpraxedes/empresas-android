package com.empresas.models;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RespostaLogin {


    @SerializedName("sucess")
    private boolean sucess;

    @SerializedName("error")
    private ArrayList<String> errors;

    @SerializedName("investor")
    private Investor investor;



    public RespostaLogin(boolean sucess, ArrayList<String> errors, Investor investor) {
        this.sucess = sucess;
        this.errors = errors;
        this.investor = investor;
    }

    public Investor getInvestor() {
        return investor;
    }

    public boolean isSucess() {
        return sucess;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }
}
