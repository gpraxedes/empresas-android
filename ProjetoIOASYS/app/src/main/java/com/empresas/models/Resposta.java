package com.empresas.models;

import com.google.gson.annotations.SerializedName;

public class Resposta {

    @SerializedName("error")
    private boolean erro;

    @SerializedName("message")
    private String msg;

    public Resposta(boolean erro, String msg){
        this.erro = erro;
        this.msg = msg;

    }

    public boolean isErro() {
        return erro;
    }

    public String getMsg() {
        return msg;
    }
}
